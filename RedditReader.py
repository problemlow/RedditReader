from pyfiglet import figlet_format
from termcolor import cprint
from colorama import init
import pyttsx3
import praw
import time
import csv
import sys
import re
# import pymongo
#
#
# myclient = pymongo.MongoClient("mongodb://localhost:27017/")
# mydb = myclient["RedditReader"]
#
# dblist = myclient.list_database_names()
# if "RedditReader" in dblist:
#     print("The database exists.")
#
# # Create a collection called "customers":
# mycol = mydb["customers"]
# temp = mydb.list_collection_names()
# mydb.main().insert("meme")
# for thing in temp:
#     print(thing)

init(strip=not sys.stdout.isatty())  # strip colors if stdout is redirected

# Setup Language Delimination Settings
alphabets = "([A-Za-z])"
prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
suffixes = "(Inc|Ltd|Jr|Sr|Co)"
starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever)"
acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
websites = "[.](com|net|org|io|gov)"


def convertSentences(text):
    text = " " + text + "  "
    text = text.replace("\n", " ")
    text = re.sub(prefixes, "\\1<prd>", text)
    text = re.sub(websites, "<prd>\\1", text)
    if "Ph.D" in text:
        text = text.replace("Ph.D.", "Ph<prd>D<prd>")
    text = re.sub("\s" + alphabets + "[.] ", " \\1<prd> ", text)
    text = re.sub(acronyms+" "+starters, "\\1<stop> \\2", text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]" + alphabets + "[.]", "\\1<prd>\\2<prd>\\3<prd>", text)
    text = re.sub(alphabets + "[.]" + alphabets + "[.]", "\\1<prd>\\2<prd>", text)
    text = re.sub(" "+suffixes+"[.] "+starters, " \\1<stop> \\2", text)
    text = re.sub(" "+suffixes+"[.]", " \\1<prd>", text)
    text = re.sub(" " + alphabets + "[.]", " \\1<prd>", text)
    if "”" in text:
        text = text.replace(".”", "”.")
    if "\"" in text:
        text = text.replace(".\"", "\".")
    if "!" in text:
        text = text.replace("!\"", "\"!")
    if "?" in text:
        text = text.replace("?\"", "\"?")
    text = text.replace(".", ".<stop>")
    text = text.replace("?", "?<stop>")
    text = text.replace("!", "!<stop>")
    text = text.replace("<prd>", ".")
    text = text.replace("&nbsp;", "")
    text = text.replace("&#x200B;", "")
    text = text.replace("**", "")
    text = text.replace("*", "")
    sentences = text.split("<stop>")
    # sentences = sentences[:-1]
    sentences = [s.strip() for s in sentences]
    return sentences


# Setup Speech Profile
engine = pyttsx3.init()
voices = engine.getProperty("voices")
engine.setProperty("voice", "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-GB_HAZEL_11.0")
engine.setProperty("rate", 800)

# Setup Reddit Authentication Info


def getAuthAndPackage():
    reddit = False
    regenConfig = False
    try:
        with open('config.txt', 'r') as f:
            reader = csv.reader(f)
            array = list(reader)
    except FileNotFoundError:
        cprint("Unrecoverable Error: File Not Found 'config.txt'", "white", "on_red")
        cprint("Creating template 'config.txt' in current directory", "white", "on_blue")
        configArray = ["Place these one per line after the username field leaving a space on line 9, there should be 14 lines in this file when you're done", "https://www.reddit.com/prefs/apps / - go here to generate the app code, click new app, give it a name, select script set the redirect url to http://localhost:8080", "Example of what the data should be similar too", "client_id      AA1_B2cDeFgH3i", "client_secret  1A2BcDE3FG4h5i6jk7lMNOPqRsT", "password       Your Password", "user_agent     ApplicationTitle by / u/yourusername", "username       Your Username", "replaceWithClient_id", "replaceWithClient_secret", "replaceWithPassword", "replaceWithUser_agent", "replaceWithUsername"]
        try:
            f = open("config.txt", "w+")
            for item in configArray:
                f.append(item+"\n")
                print("added: "+item)
            f.close()
            regenConfig = True
        except e as e:
            cprint("Unrecoverable Error: Unhandled exception"+string(e), "white", "on_red")
    try:
        client_id = array[9][0]
        client_secret = array[10][0]
        password = array[11][0]
        user_agent = array[12][0]
        username = array[13][0]
        cprint("   Logged in as: "+username+"   ", "white", "on_blue")
        reddit = praw.Reddit(client_id=client_id, client_secret=client_secret, password=password, user_agent=user_agent, username=username)
    except IndexError:
        cprint("Unrecoverable Error: 'IndexError' Try deleting your 'config.txt'", "white", "on_red")
    except UnboundLocalError:
        cprint("Unrecoverable Error: 'UnboundLocalError' Try deleting your 'config.txt'", "white", "on_red")
    return(reddit)


def checkID(id):
    try:
        with open('ids.txt', 'r') as f:
            reader = csv.reader(f)
            array = list(reader)
    except FileNotFoundError:
        cprint("Error: File not found ids.txt", "white", "on_red")
    for line in array:
        if id == line[0]:
            return(True)


def speak(engine, text):
    engine.say(text)
    engine.runAndWait()
    engine.stop()

def loadCsv():
    with open('ids.txt', 'r') as f:
        reader = csv.reader(f)
        array = list(reader)
    return(array)


def main(pastX, startingPostNumber, numPostsToRetreve, subreddit):
    postNumber = 1
    csv = loadCsv()
    for submission in reddit.subreddit(subreddit).top(limit=numPostsToRetreve, time_filter=pastX):
        skip = False
        inCsv = False
        for value in csv:
            # print(value[0]+" --=-- "+submission.permalink)
            if value[0] == submission.permalink:
                inCsv = True
                # print("SET INCSV TO TRUE")
                break
        if inCsv == True:
            cprint("Skipping post "+str(postNumber)+" due to id already in csv.", "blue", "on_yellow")
            postNumber += 1
        elif postNumber < startingPostNumber:
            cprint("Skipping post "+str(postNumber)+" due to Starting Post Number Speicifed as. "+str(startingPostNumber), "blue", "on_yellow")
            # print("Incrementing post number", postNumber)
            postNumber += 1
        elif submission.is_meta == True:
            cprint("Skipping post "+str(postNumber)+" due to having already interacted with this post via up/down vote.", "blue", "on_yellow")
            # print("Incrementing post number", postNumber)
            postNumber += 1
        elif submission.likes != None:
            cprint("Skipping post "+str(postNumber)+" due to having already interacted with this post via up/down vote.", "blue", "on_yellow")
            # print("Incrementing post number", postNumber)
            postNumber += 1
        elif submission.id == "3dqlny":
            cprint("Skipping post "+str(postNumber)+" due to FUCKING TALES FROM TECH SUPPORT PINNED POSTS.", "blue", "on_yellow")
            # print("Incrementing post number", postNumber)
            postNumber += 1
        else:
            text = None
            sentences = None
            cprint(figlet_format("    START "+str(postNumber)+"    ", font="big"), "white", "on_blue", attrs=["bold"])
            print("PostID:", str(submission.id))
            print("PostAuthor:", str(submission.author))
            print("PostUps:", str(submission.ups))
            print("PostDowns:", str(submission.downs))
            speak(engine, "Post Start")
            cprint("https://www.reddit.com"+submission.permalink, "blue", "on_white")
            print("")
            print("Title: "+submission.title)
            speak(engine, "Title: "+submission.title)
            text = submission.selftext
            sentences = convertSentences(text)
            firstSentence = True
            # print(submission.visited)
            # for thing in dir(submission):
            #     print(thing)
            for sentence in sentences:
                if firstSentence == True:
                    firstSentence = False
                    sentence = "Post: "+sentence
                print(sentence)
                if skip == False:
                    speak(engine, sentence)
                else:
                    break
            try:
                if submission.ups > 0:
                    submission.upvote()  # AutoUpVote
                    cprint("   UpVoted Post "+str(postNumber)+"   ", "white", "on_green")
                elif submission.downs > submission.ups and submission.ups > 0:
                    submission.downvote()
                    cprint("   DownVoted Post "+str(postNumber)+"   ", "white", "on_green")
            except:
                cprint("   Post Voting Preference Failed for Post "+str(postNumber)+" This is most likely due to the post being archived   ", "white", "on_red")
                cprint("   Here is the url of the post for you to check if my hypothesis is correct:   ", "white", "on_red")
                cprint("https://www.reddit.com"+submission.permalink, "blue", "on_yellow")
            if inCsv == False:
                try:
                    with open("ids.txt", "a") as f:
                        f.write("\n"+submission.permalink)
                except UnicodeEncodeError as e:
                    print(submission.permalink)
                    cprint("   "+str(e)+"   ", "white", "on_red")
                    # return()
                cprint("   Post "+str(postNumber)+" added to viewedlist   ", "white", "on_green")
            print("################################################################################")
            speak(engine, "Post End")
            # cprint(figlet_format("        END        "+str(postNumber), font="big"), "white", "on_blue", attrs=["bold"])
            postNumber += 1
            # print("Incrementing post number", postNumber)
        # print("END OF MAIN FOR INSIDE MAIN")
    return(postNumber)


reddit = getAuthAndPackage()
AuthCheck = True
if reddit == False:
    AuthCheck = False
while AuthCheck:
    option = "hio"
    option = input("""Defaults:
Number of posts: 8192
Starting post number: 1
Posts from the last: all
Subreddit: TalesFromTechSupport, if invalid

Change Defaults? (Yes/No)
Any option where a value is not selected will be set to the default
Enter Choice: """)
    if option == "No" or option == "N" or option == "no" or option == "n":
        print("Continuing with default settings")
        pastX = "all"
        startingPostNumber = 0
        numPostsToRetreve = 8192
        try:
            print("")
            print("Enter the name of the subreddit you want read to you as it appears in the url to it with no spaces")
            subreddit = input("Enter Subreddit Name: ")
            if len(subreddit) < 3:
                0/0
            if len(subreddit) > 20:
                0/0
        except:
            print("Defaulting to TalesFromTechSupport")
            subreddit = "TalesFromTechSupport"
    elif option == "Yes" or option == "Y" or option == "yes" or option == "y":
        try:
            print("")
            print("Enter the name of the subreddit you want read to you as it appears in the url to it with no spaces")
            subreddit = input("Enter Subreddit Name: ")
            if len(subreddit) < 3:
                0/0
            if len(subreddit) > 20:
                0/0
        except:
            print("Defaulting to TalesFromTechSupport")
            subreddit = "TalesFromTechSupport"
        print("")
        print("Retreve posts from the past: hour, day, week, month, year, all")
        pastX = input("Enter Choice from options shown above: ")
        print(pastX)
        if pastX != "hour" and pastX.lower() != "day" and pastX.lower() != "week" and pastX.lower() != "month" and pastX.lower() != "year" and pastX.lower() != "all":
            print("You didnt enter your choice correctly. Displaying posts from the last all")
            pastX = "all"
        try:
            print("")
            print("Start reading from post number")
            startingPostNumber = int(input("Enter Choice As A Number: "))
        except:
            print("You didnt enter your choice correctly. Starting from post 0")
            startingPostNumber = 0
        try:
            print("")
            print("Enter a number of posts greater than your starting post number")
            numPostsToRetreve = int(input("Enter Choice As A Number: "))
        except:
            print("You didnt enter your choice correctly. Setting number of posts to retreve to starting post number + 8192")
            numPostsToRetreve = startingPostNumber + 8192
    else:
        print("Continuing with default settings")
        pastX = "all"
        startingPostNumber = 0
        numPostsToRetreve = 8192
    try:
        startingPostNumber = main(pastX, startingPostNumber, numPostsToRetreve, subreddit)
    except NameError:
        print("### Subreddit is not defined exiting ###")

try:
    print("Loop Terminated This Is Probably Fine", startingPostNumber)
except:
    print("Never Run? Probably not fine.")
